/*******************************************************************************
 * Copyright 2021 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/


#ifndef CONFIG_FILE_H
#define CONFIG_FILE_H

#include <modal_json.h>
#include <modal_pipe_client.h>

// config file exists here if you want to access it
#define VOXL_UVC_SERVER_CONF_FILE	"/etc/modalai/voxl-uvc-server.conf"

#define CONFIG_FILE_HEADER "\
/**\n\
 * This file contains configuration that's specific to voxl-uvc-server.\n\
 * parameter descriptions:\n\
 * \n\
 * width: Desired image width. (e.g. 640)\n\
 * Default is 640.\n\
 *\n\
 * height: Desired image height. (e.g. 480)\n\
 * Default is 480.\n\
 *\n\
 * height: Desired frame rate in fps (e.g. 15).\n\
 * Default is 30.\n\
 *\n\
 * pipe-name: Desired MPA pipe name in /run/mpa (e.g. webcam).\n\
 * Default is uvc.\n\
 *\n\
 */\n"

static char pipe_name[MODAL_PIPE_MAX_NAME_LEN];	// pipe name read from config file
static int width; // Frame width
static int height; // Frame height
static int fps; // Frames per second

/**
 * load the config file and populate the above extern variables
 *
 * @return     0 on success, -1 on failure
 */
static void config_file_print(void)
{
	printf("=================================================================\n");
    printf("width:                            %d\n",    width);
	printf("height:                           %d\n",    height);
    printf("fps:                              %d\n",    fps);
	printf("pipe_name:                        %s\n",    pipe_name);
	printf("=================================================================\n");
	return;
}

/**
 * @brief      prints the current configuration values to the screen
 *
 *             this includes all of the extern variables listed above. If this
 *             is called before config_file_load then it will print the default
 *             values.
 */
static int config_file_read(void)
{
	int ret = json_make_empty_file_with_header_if_missing(VOXL_UVC_SERVER_CONF_FILE, CONFIG_FILE_HEADER);
	if(ret < 0) return -1;
	else if(ret>0) fprintf(stderr, "Creating new config file: %s\n", VOXL_UVC_SERVER_CONF_FILE);

	cJSON* parent = json_read_file(VOXL_UVC_SERVER_CONF_FILE);
	if(parent==NULL) return -1;
 
	json_fetch_string_with_default(	parent, "pipe_name",	pipe_name,  MODAL_PIPE_MAX_NAME_LEN,	"uvc");
	json_fetch_int_with_default(	parent, "width",		&width,		640);
    json_fetch_int_with_default(	parent, "height",		&height,	480);
    json_fetch_int_with_default(	parent, "fps",		    &fps,	    30);

	if(json_get_parse_error_flag()){
		fprintf(stderr, "failed to parse config file %s\n", VOXL_UVC_SERVER_CONF_FILE);
		cJSON_Delete(parent);
		return -1;
	}

	// write modified data to disk if necessary
	if(json_get_modified_flag()){
		//printf("The config file was modified during parsing, saving the changes to disk\n");
		json_write_to_file_with_header(VOXL_UVC_SERVER_CONF_FILE, parent, CONFIG_FILE_HEADER);
	}
	cJSON_Delete(parent);
	return 0;
}

#endif // end #define CONFIG_FILE_H
